function onResponse(response) {
    console.log("Received " + response);
    browser.notifications.create({
	"type": "basic",
	"title": response,
	"message": "Link added to shiori",
	"iconUrl": browser.extension.getURL("icons/shiori.png")
  });
}

function onError(error) {
    console.log(`Error: ${error}`);
}

/*
On a click on the browser action, send the app a message.
*/
browser.browserAction.onClicked.addListener(() => {
    var gettingActiveTab = browser.tabs.query({active: true, currentWindow: true});
    gettingActiveTab.then((tabs) => {
	console.log("Sending " + tabs[0].url);
	 var sending = browser.runtime.sendNativeMessage(
	     "shiori",
	     tabs[0].url);
	sending.then(onResponse, onError);
    });
});


